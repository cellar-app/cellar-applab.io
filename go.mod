module gitlab.com/cellar-app/cellar-app.gitlab.io

go 1.22.8

require gitlab.com/cellar-app/hugo-theme-cellar v0.3.1 // indirect
