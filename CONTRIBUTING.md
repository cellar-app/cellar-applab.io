# Contributing to the Cellar Documentation

First of all, thank you for your desire to contribute!
It is sincerely appreciated!

> Note: The primary location for contributing to the project is on GitLab.
> It is mirrored to other locations for visibility.
> If you would like to contribute, start by navigating to [this document on GitLab][contributing-gitlab].

## Cellar Issues and Features

This repositiory contains only the Cellar Documentation website.
As such issues for this repository should pertain only to the documentation.

If you would like to request a new feature or report an issue with the cellar application functionality,
you may do so with either the [API][cellar-api] or [UI][cellar-ui] repository.


### Adding a Language

We would love to have the Cellar language translated into as many languages as possible!

If there is a language to which you would like to have the Cellar documentation translated,
[create an issue][issues-new].


If you are able and would like to translate Cellar documentation into another language,
add a comment on the issue for that language.

A member of the Cellar team will assign the issue to you.
It may take a while for the issue to be assigned to you, but in the meantime,
if you were the first to comment to claim the issue, you can begin working.


### Reporting Issues

Reporting issues is a great way to contribute!
We appreciate detailed issue reports.
However, before reporting an issue, please make sure it doesn't already exist in the [issues list][issues-list].

If an issue does exist, please refrain from commenting "+1" or similar comments.
That said, if you have addditional context, such as new ways to reproduce an issue, please to leave a comment.


## Developing the Documentation Website

The cellar documentation website is a static site generated using [Hugo][hugo].

It uses the [Learn theme for Hugo][hugo-theme-learn].

You can see the latest hosted version at [https://cellar-app.io][docs-home].


### Local Dependencies

Working on this website requires only a text editor.
Though, as all the documentation is written in [markdown][markdown], a text editor with markdown syntax support is recommended.

While not entirely necessary, it may be useful to run the documentation website locally.
To do so you will either need to either have [Hugo][hugo] installed locally or have [Docker][docker] and [docker-compose][docker-compose] installed.


### Getting started

To run the site with [Docker][docker]:

```shell
docker-compose up
```

To run the site with [Hugo][hugo]:

```shell
hugo server
```

In either case you should now be able to access the website at http://127.0.0.1:1313.
Any changes you make to the website while the site is running should be picked up automatically.


### Project Structure

This project is structured according to the [Hugo directory structure][hugo-directory-structure].

However, in most cases you should only need to make changes to the content directory.
This is where all of the documentation that appears on the website is stored.
Simply make a change to the markdown files in that directory.

To make any major changes to the website structure, please read the [Learn theme documenation][hugo-theme-learn].


## Hugo Version

To update the version of [Hugo][hugo] used for generation, update the `image` line of the [docker-compose.yml][docker-compose-yml] as well as the `image` line of the [.gitlab-ci.yml][gitlab-ci-yml].


## Deployment and Versioning

Any any changes merged or committed to main are autiomatically deployed to the [hosted website][docs-home] through the [GitLab pipeline][gitlab-pipeline].



[docs-home]: https://cellar-app.io

[gitlab-ci-yml]: .gitlab-ci.yml
[docker-compose-yml]: docker-compose.yml

[issues-list]: https://gitlab.com/cellar-app/cellar-app.gitlab.io/-/issues
[issues-new]: https://gitlab.com/cellar-app/cellar-app.gitlab.io/-/issues/new

[contributing-gitlab]: https://gitlab.com/cellar-app/cellar-app.gitlab.io/-/blob/main/CONTRIBUTING.md
[gitlab-pipeline]: https://gitlab.com/cellar-app/cellar-app.gitlab.io/-/pipelines
[cellar-api]: https://gitlab.com/cellar-app/cellar-api
[cellar-ui]: https://gitlab.com/cellar-app/cellar-ui

[docker]: https://www.docker.com/
[docker-compose]: https://docs.docker.com/compose/
[hugo]: https://cellar-app.io/
[markdown]: https://www.markdownguide.org/
[hugo-theme-learn]: https://learn.netlify.app/en/
[hugo-directory-structure]: https://gohugo.io/getting-started/directory-structure/
