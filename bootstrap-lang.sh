#!/usr/bin/env bash
LANG_CODE=$1

usage() {
    echo "$0 LANG_CODE"
    exit 1
}

[ -z "${LANG_CODE}" ] && usage

files=$(find content/ -name '*.md')
for file in ${files}; do
    new_file="${file/\.en\./\.${LANG_CODE}\.}"
    [ ! -f "$new_file" ] && cp "${file}" "${new_file}" || echo "'${new_file}' already exists skipping..."
done

