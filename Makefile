GREEN := "\033[0;32m"
NC := "\033[0;0m"

LOG := @sh -c '\
	   printf ${GREEN}; \
	   echo -e "\n> $$1\n"; \
	   printf ${NC}' VALUE


HUGO_IMAGE ?= registry.gitlab.com/pages/hugo
HUGO_VERSION ?= 0.136.5
HUGO_PORT ?= 1313
HUGO_HOST_PORT ?= ${HUGO_PORT}

HUGO := docker run \
			--rm \
			-it \
			-v ./:/cellar \
			-w /cellar \
			-p ${HUGO_HOST_PORT}:${HUGO_PORT} \
			${HUGO_IMAGE}:${HUGO_VERSION} \
			hugo

NAME ?=


COMMAND ?=

set-theme-local::
	$(LOG) "Setting theme to local path"
	@yq -iy '.module.imports.[0].path = "../../hugo-theme-cellar"' config/_default/config.yaml

set-theme-published::
	$(LOG) "Setting theme to published"
	@yq -iy '.module.imports.[0].path = "gitlab.com/cellar-app/hugo-theme-cellar"' config/_default/config.yaml

up-local-theme:: set-theme-local
	$(LOG) "Starting hugo server with local theme"
	@hugo server --disableFastRender --watch ../hugo-theme-cellar/

up:: set-theme-published
	$(LOG) "Starting hugo server"
	@hugo server --bind 0.0.0.0 --port ${HUGO_PORT}

load-theme::
	@hugo mod tidy

update-theme:: set-theme-published
	@hugo mod get -u

up-docker::
	$(LOG) "Starting hugo server"
	$(hugo) server --bind 0.0.0.0 --port ${HUGO_PORT}

hugo-docker::
	$(LOG) "Running hugo command"
	$(HUGO) ${COMMAND}
