+++
title = "Розгортання"
date =  2020-11-28T23:56:10Z
weight = 5
+++

## Cellar Інтерфейс (Cellar UI)

Щоб розгорнути сам Cellar інтерфейс можна або завантажити мініфікований код на веб-сервері або запустити контейнер Docker.
Можна завантажити код на формат архів `.tgz` на [сторінці релізів][ui-releases].
Останнє зображення Docker можна знайти в [Реєстрі контейнерів GitLab][ui-registry].

Щоб запустити програму, просто запустіть образ Docker.
--Веб-сервер в контейнері розпочне слухати через порт 80.

Якщо ви використовуєте архів `.tgz`, вам потрібно буде самостійно встановити свій веб-сервер.

Також ви можете самостійно скласти код інтерфейса.
Для отримання додаткової інформації про це див [вказівки щодо внесків проекта Cellar UI][ui-contributing].


## Cellar ППІ (Прикладний Програмний Інтерфейс)

Сам Cellar ППІ можна розгорнути або як виконуваний файл або як контейнер Docker.
Останній виконуваний файл можна завантажити на [сторінці релізів][api-releases].
Останній образ Docker можна знайти в [Реєстрі контейнерів GitLab][api-registry].

Просто запустіть виконуваний файл чи контейнер.
ППІ приймає зв'язок на ту адресу, яку ви налаштували (див `APP_BIND_ADDRESS` вниз).

Також ви можете самостійно скласти код ППІ.
Для отримання додаткової інформації про це див [вказівки щодо внесків проекта Cellar ППІ][api-contributing].


## Система Зберігання Даних (Datastore)

### Redis

Не потрібно ніякий особлива налаштування.
Ви можете створити пароль для, але це не обов'язково.


## Криптографія (Cryptography)

### AWS KMS

AWS KMS Vault потрібно налаштування до того як Cellar може з ним працювати.

Спочатку ви повинні створити [KMS Key][aws-kms-key].
Наступно треба створити IAM policy яка має [дозвіль][aws-kms-permissions] шифрувати і дешрифрувати з таким KMS key.
Останні, надайте цю політику AWS KMS Key (ймовірно, створивши AWS role і надавши її інфраструктурі, де ви розгорнули Cellar ППІ).

### Vault

Vault потрібно налаштування до того як Cellar може з ним працювати.

По перші потрібно метод автентифікація.
Зараз Cellar тільки підтримує метод [AppRole][vault-approle].
Також потрібно створити рол в метод AppRole.

Наступне треба увімкнути [Transit Secrets Engine][vault-transit] і згенерувати ключ.
Рол має мати доступ зашифрувати та розшифрувати з цим ключом.

Для отримання додаткової інформації про налаштування Vault див [Документація Vault][vault-docs].


[api-releases]: https://gitlab.com/auroq/cellar/cellar-api/-/releases

[api-registry]: https://gitlab.com/auroq/cellar/cellar-api/container_registry

[api-contributing]: https://gitlab.com/auroq/cellar/cellar-api/-/blob/32010035b70f5c675a1ef12953de411dccd492a3/CONTRIBUTING.md

[ui-releases]: https://gitlab.com/auroq/cellar/cellar-ui/-/releases

[ui-registry]: https://gitlab.com/auroq/cellar/cellar-ui/container_registry

[ui-contributing]: https://gitlab.com/auroq/cellar/cellar-ui/-/blob/63327848ada49004e4ad8d3d2157f54e90f62978/CONTRIBUTING.md

[ui]: https://gitlab.com/auroq/cellar/cellar-ui

[redis]: https://redis.io/

[vault]: https://www.vaultproject.io/

[vault-docs]: https://www.vaultproject.io/docs

[vault-approle]: https://www.vaultproject.io/docs/auth/approle

[vault-transit]: https://www.vaultproject.io/docs/secrets/transit

[aws-kms-key]: https://docs.aws.amazon.com/kms/latest/developerguide/concepts.html

[aws-kms-permissions]: https://docs.aws.amazon.com/kms/latest/developerguide/kms-api-permissions-reference.html
