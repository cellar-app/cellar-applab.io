+++
title = "v3.x.x (Current)"
weight = 2
+++

> [!INFO]
> This documentation is up to date as of version 3.x.x.

All cellar configuration is handled through API settings,
and such API configuration handled through environment variables.

You must configure both the application settings as well as one datastore and one cryptography engine.
See below for details.

## App

| Name                               | Type        | Description                                                                                                                                                       | Default                 |
|:-----------------------------------|:------------|:------------------------------------------------------------------------------------------------------------------------------------------------------------------|:------------------------|
| **`APP_CLIENT_ADDRESS`**           | **string**  | Specifies the fully qualified domain name where Cellar will be hosted.                                                                                            | `http://127.0.0.1:8080` |
| **`APP_BIND_ADDRESS`**             | **string**  | Specifies the address on which the Cellar API will listen for requests.                                                                                           | `127.0.0.1:8080`        | 
| **`LOGGING_LEVEL`**                | **string**  | Specifies the minimum level of logs that Cellar will output. Must be one of the following values: `trace`, `debug`, `info`, `warning`, `error`, `fatal`, `panic`. | `info`                  |
| **`LOGGING_ENABLE_STDOUT`**        | **boolean** | Specifies whether Cellar should output logs to standard out.                                                                                                      | `true`                  |
| **`LOGGING_DIRECTORY`** (optional) | **string**  | Specifies a directory where Cellar should write logs. Cellar will create the directory if it does not exist.                                                      |                         |

## Datastore

All datastore settings are contained in a "datastore" section

> [!NOTE]
> Since there is currently only one datastore, it is enabled by default.

### Redis

| Name                                      | Type        | Description                                                    | Default     |
|:------------------------------------------|:------------|:---------------------------------------------------------------|:------------|
| **`DATASTORE_REDIS_HOST`**                | **string**  | Specifies the host of the Redis database.                      | `localhost` |
| **`DATASTORE_REDIS_PORT`**                | **integer** | Specifies the host one which to connect to the Redis database. | `6379`      |
| **`DATASTORE_REDIS_PASSWORD`** (optional) | **string**  | Specifies the password for the Redis database.                 |             |
| **`DATASTORE_REDIS_DB`**                  | **integer** | Specifies which database to connect to on the Redis instance.  | `0`         |


## Cryptography

All cryptography settings are contained in a "cryptography" section.
Each of them contains an "enabled" option.
It must be set to true for that cryptography engine to be enabled.
However, only one engine can be enabled at a time.

### AWS KMS

| Name                              | Type        | Description                                                                                             | Default  |
|:----------------------------------|:------------|:--------------------------------------------------------------------------------------------------------|:---------|
| **`CRYPTOGRAPHY_AWS_ENABLED`**    | **boolean** | Enables AWS as a cryptography engine                                                                    | `false`  |
| **`CRYPTOGRAPHY_AWS_REGION`**     | **string**  | Specifies the [AWS region][aws-region] where your kms key is located.                                   |          |
| **`CRYPTOGRAPHY_AWS_KMS_KEY_ID`** | **string**  | Specifies the [ID][kms-key-id] of the [KMS key][kms-key] from AWS to use for encrypting and decrypting. |          |

### Vault

| Name                                           | Type        | Description                                                                                                                            | Default                 |
|:-----------------------------------------------|:------------|:---------------------------------------------------------------------------------------------------------------------------------------|:------------------------|
| **`CRYPTOGRAPHY_VAULT_ENABLED`**               | **boolean** | Enables vault as a cryptography engine                                                                                                 | `false`                 |
| **`CRYPTOGRAPHY_VAULT_ADDRESS`**               | **string**  | Specifies the full address to the Vault instance.                                                                                      | `http://localhost:8200` |
| **`CRYPTOGRAPHY_VAULT_ENCRYPTION_TOKEN_NAME`** | **string**  | Specifies the name of the encryption key from the [Vault Transit Secrets ENgine][vault-transit] to use for encrypting and decrypting.  |                         |
| **`CRYPTOGRAPHY_VAULT_AUTH_MOUNT_PATH`**       | **string**  | Specifies the mount path of the auth engine you whish to use to authenticate from Cellar API to vault (see below for more information) |                         |


Cellar supports multiple methods of authentication between your api and vault.
You must configure exactly one of them.

For more information on how each of these auth methods work, please refer to the [Vault Auth Methods documentation][vault-auth-methods].

#### AppRole Auth
| Name                               | Type       | Description                                                                               | Default  |
|:-----------------------------------|:-----------|:------------------------------------------------------------------------------------------|:---------|
| **`CRYPTOGRAPHY_VAULT_ROLE_ID`**   | **string** | Specifies the `role_id` to use for authenticating to Vault with AppRole authentication.   |          |
| **`CRYPTOGRAPHY_VAULT_SECRET_ID`** | **string** | Specifies the `secret_id` to use for authenticating to Vault with AppRole authentication. |          |

#### AWS IAM Auth
| Name                                      | Type       | Description                                                                                  | Default  |
|:------------------------------------------|:-----------|:---------------------------------------------------------------------------------------------|:---------|
| **`CRYPTOGRAPHY_VAULT_AUTH_AWSIAM_ROLE`** | **string** | Specifies the AWS IAM `role` to use for authenticating to vault with AWS IAM authentication. |          |

#### GCP IAM Auth
| Name                                      | Type       | Description                                                                                  | Default  |
|:------------------------------------------|:-----------|:---------------------------------------------------------------------------------------------|:---------|
| **`CRYPTOGRAPHY_VAULT_AUTH_GCPIAM_ROLE`** | **string** | Specifies the GCP IAM `role` to use for authenticating to vault with GCP IAM authentication. |          |


#### Kubernetes  Auth
| Name                                          | Type       | Description                                                                          | Default  |
|:----------------------------------------------|:-----------|:-------------------------------------------------------------------------------------|:---------|
| **`CRYPTOGRAPHY_VAULT_AUTH_KUBERNETES_ROLE`** | **string** | Specifies the K8s `role` to use for authenticating to vault with K8s authentication. |          |


[vault-transit]: https://www.vaultproject.io/docs/secrets/transit

[vault-auth-methods]: https://developer.hashicorp.com/vault/docs/auth

[aws-region]: https://aws.amazon.com/about-aws/global-infrastructure/regions_az/

[kms-key]: https://aws.amazon.com/kms/

[kms-key-id]: https://docs.aws.amazon.com/kms/latest/developerguide/find-cmk-id-arn.html
