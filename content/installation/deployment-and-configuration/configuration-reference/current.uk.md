+++
title = "v3.x.x (поточна версія)"
weight = 2
+++

> [!INFO]
> This documentation is up to date as of version 3.x.x.

All cellar configuration is handled through API settings,
and such API configuration handled through environment variables.

You must configure both the application settings as well as one datastore and one cryptography engine.
See below for details.

## Додаток

| Name                               | Type        | Description                                                                                                                                  | Default                 |
|:-----------------------------------|:------------|:---------------------------------------------------------------------------------------------------------------------------------------------|:------------------------|
| **`APP_CLIENT_ADDRESS`**           | **string**  | Визначає FQDN (повністю визначення ім'я домена) куди ви розгортаєте Cellar.                                                                  | `http://127.0.0.1:8080` |
| **`APP_BIND_ADDRESS`**             | **string**  | Визначає адресу на який Cellar буде примати зв'язок. За замовчуванням.                                                                       | `127.0.0.1:8080`        | 
| **`LOGGING_LEVEL`**                | **string**  | Визначає мінімальний рівень серйозності для ведення журналу. Має бути один з `trace`, `debug`, `info`, `warning`, `error`, `fatal`, `panic`. | `info`                  |
| **`LOGGING_ENABLE_STDOUT`**        | **boolean** | Визначає чи Cellar повинен писати логи в стандартне виведення (stdout).                                                                      | `true`                  |
| **`LOGGING_DIRECTORY`** (optional) | **string**  | Визначає папку туда, куда Cellar повинен писати логи. Cellar створює папку якщо її немає.                                                    |                         |


## Система Зберігання Даних (Datastore)

All datastore settings are contained in a "datastore" section

> [!NOTE]
> Since there is currently only one datastore, it is enabled by default.

### Redis

| Name                            | Type        | Description                                                               | Default     |
|:--------------------------------|:------------|:--------------------------------------------------------------------------|:------------|
| **`REDIS_HOST`**                | **string**  | Визначає хост на яких база даних Redis приймає зв'язки.                   | `localhost` |
| **`REDIS_PORT`**                | **integer** | Визначає порт на яких база даних Redis приймає зв'язки.                   | `6379`      |
| **`REDIS_PASSWORD`** (optional) | **string**  | Це пароль від база даних Redis.                                           |             |
| **`REDIS_DB`**                  | **integer** | Визначає номер база даних в екземпляр Redis з яким ви хочете взаємодіяти. | `0`         |


## Криптографія (Cryptography)

All cryptography settings are contained in a "cryptography" section.
Each of them contains an "enabled" option.
It must be set to true for that cryptography engine to be enabled.
However, only one engine can be enabled at a time.

### AWS KMS

| Name                              | Type        | Description                                                                                             | Default  |
|:----------------------------------|:------------|:--------------------------------------------------------------------------------------------------------|:---------|
| **`CRYPTOGRAPHY_AWS_ENABLED`**    | **boolean** | Enables AWS as a cryptography engine                                                                    | `false`  |
| **`CRYPTOGRAPHY_AWS_REGION`**     | **string**  | Specifies the [AWS region][aws-region] where your kms key is located.                                   |          |
| **`CRYPTOGRAPHY_AWS_KMS_KEY_ID`** | **string**  | Specifies the [ID][kms-key-id] of the [KMS key][kms-key] from AWS to use for encrypting and decrypting. |          |

### Vault

| Name                              | Type       | Description                                                                                                                            | Default                 |
|:----------------------------------|:-----------|:---------------------------------------------------------------------------------------------------------------------------------------|:------------------------|
| **`VAULT_ADDRESS`**               | **string** | Визначає вся адреса екземпляр Vault.                                                                                                   | `http://localhost:8200` |
| **`VAULT_ENCRYPTION_TOKEN_NAME`** | **string** | Визначає назва ключа з [Vault Transit Secrets Engine][vault-transit] який Cellar буде використовувати щоб зашифрувати та розшифрувати. |                         |
| **`VAULT_AUTH_MOUNT_PATH`**       | **string** | Specifies the mount path of the auth engine you whish to use to authenticate from Cellar API to vault (see below for more information) |                         |


Cellar supports multiple methods of authentication between your api and vault.
You must configure exactly one of them.

For more information on how each of these auth methods work, please refer to the [Vault Auth Methods documentation][vault-auth-methods].

#### AppRole Auth
| Name                  | Type       | Description                                                                                                           | Default  |
|:----------------------|:-----------|:----------------------------------------------------------------------------------------------------------------------|:---------|
| **`VAULT_ROLE_ID`**   | **string** | Визначає те, який `role_id` використовувати щоб увійти в Vault з [з методом автентифікації AppRole][vault-approle].   |          |
| **`VAULT_SECRET_ID`** | **string** | Визначає те, який `secret_id` використовувати щоб увійти в Vault з [з методом автентифікації AppRole][vault-approle]. |          |

#### AWS IAM Auth
| Name                         | Type       | Description                                                                                  | Default  |
|:-----------------------------|:-----------|:---------------------------------------------------------------------------------------------|:---------|
| **`VAULT_AUTH_AWSIAM_ROLE`** | **string** | Specifies the AWS IAM `role` to use for authenticating to vault with AWS IAM authentication. |          |

#### GCP IAM Auth
| Name                         | Type       | Description                                                                                  | Default  |
|:-----------------------------|:-----------|:---------------------------------------------------------------------------------------------|:---------|
| **`VAULT_AUTH_GCPIAM_ROLE`** | **string** | Specifies the GCP IAM `role` to use for authenticating to vault with GCP IAM authentication. |          |


#### Kubernetes  Auth
| Name                             | Type       | Description                                                                          | Default  |
|:---------------------------------|:-----------|:-------------------------------------------------------------------------------------|:---------|
| **`VAULT_AUTH_KUBERNETES_ROLE`** | **string** | Specifies the K8s `role` to use for authenticating to vault with K8s authentication. |          |


[vault-transit]: https://www.vaultproject.io/docs/secrets/transit

[vault-auth-methods]: https://developer.hashicorp.com/vault/docs/auth

[aws-region]: https://aws.amazon.com/about-aws/global-infrastructure/regions_az/

[kms-key]: https://aws.amazon.com/kms/

[kms-key-id]: https://docs.aws.amazon.com/kms/latest/developerguide/find-cmk-id-arn.html
