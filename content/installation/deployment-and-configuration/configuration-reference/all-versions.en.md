+++
title = "All Versions"
weight = 1
+++

All cellar configuration is handled through API settings.

You must configure both the application settings as well as one datastore and one cryptography engine.

For more information please refer to the configuration reference documentation for your version of cellar.


| API VERSION                   | NOTES   |
|:------------------------------|:--------|
| [v3.x.x]({{% ref current %}}) | CURRENT |
| [v2.x.x]({{% ref v2.x.x %}})  |         |
| [v1.x.x]({{% ref v1.x.x %}})  |         |
