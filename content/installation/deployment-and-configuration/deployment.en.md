+++
title = "Deployment"
date = 2020-11-28T23:56:10Z
weight = 2
+++

## Cellar UI

The UI itself can be deployed by hosting the minified source with a webserver or as a Docker image.
A `.tgz` archive containing the latest minified source can be found on the [releases page][ui-releases].
The latest Docker image can be found in the [GitLab registry][ui-registry].

Simply start the docker iamge to start the application.
It has a webserver built in and will start running on port 80 inside the container.

If you you opt for the `.tgz` archive, you will need to use your own webserver to host it.

Alternatively you can build the UI yourself from source.
For more information on building the application, refer to the [ui project contributing documentation][ui-contributing].

The UI will not function without access to the cellar API.
It expects to be able to access the api from the path `/api`.
For example, if you are hosting the Cellar UI on `https://cellar.example.com`,
it will attempt to make all api requests to `https://cellar.example.com/api/...`
No additional configuration is necessary.

## Cellar API

The API itself can be deployed as a standalone binary or as a Docker image.
The latest binary can be found on the [releases page][api-releases].
The latest Docker image can be found in the [GitLab registry][api-registry].

Simply run the standalone binary or start the docker image.
It will start listening on the `APP_BIND_ADDRESS` that you specify in your configuration.

Alternatively you can build the API yourself from source.
For more information on building the application, refer to
the [api project contributing documentation][api-contributing].

## Datastore

Cellar requires somewhere to store application data.
Please refer to each section below for each supported datastore.

### Redis

Cellar does not require any special configuration for Redis.
Password is optional, but supported.

## Cryptography

Cellar does not do its own encryption.
Instead it relies on whatever encryption service you already trust!
Please refer to each section below for each supported cryptography engine.

### AWS KMS

AWS KMS requires some pre-existing setup to work with Cellar.

First you must create a [KMS key][aws-kms-key].
Then you must create an IAM policy that has [permissions][aws-kms-permissions] to the key for both encryption and decryption.
Finally provide this policy to the key (probably by creating a role and providing it to the infrastructure where you are running the Cellar api).

### Vault

Vault requires some pre-existing setup to work with Cellar.

First it must have an authentication method.
Currently the only supported auth method is [AppRole][vault-approle].
A role must be setup in the AppRole auth mehod.

It must also have the [Transit Secrets Engine][vault-transit] enabled and have a key generated.
The role must be given the proper capabilities to encrypt and decrypt using that key.

For more information on setting up Vault refer to [Vault's documentation][vault-docs].


[api-releases]: https://gitlab.com/auroq/cellar/cellar-api/-/releases

[api-registry]: https://gitlab.com/auroq/cellar/cellar-api/container_registry

[api-contributing]: https://gitlab.com/auroq/cellar/cellar-api/-/blob/32010035b70f5c675a1ef12953de411dccd492a3/CONTRIBUTING.md

[ui-releases]: https://gitlab.com/auroq/cellar/cellar-ui/-/releases

[ui-registry]: https://gitlab.com/auroq/cellar/cellar-ui/container_registry

[ui-contributing]: https://gitlab.com/auroq/cellar/cellar-ui/-/blob/63327848ada49004e4ad8d3d2157f54e90f62978/CONTRIBUTING.md

[ui]: https://gitlab.com/auroq/cellar/cellar-ui

[redis]: https://redis.io/

[vault]: https://www.vaultproject.io/

[vault-docs]: https://www.vaultproject.io/docs

[vault-approle]: https://www.vaultproject.io/docs/auth/approle

[vault-transit]: https://www.vaultproject.io/docs/secrets/transit

[aws-kms-key]: https://docs.aws.amazon.com/kms/latest/developerguide/concepts.html

[aws-kms-permissions]: https://docs.aws.amazon.com/kms/latest/developerguide/kms-api-permissions-reference.html
