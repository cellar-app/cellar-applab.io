+++
title = "Структура Програми"
date =  2020-11-28T23:55:59Z
weight = 3
+++

Cellar складається з чотирьох частин.
Опис кожної з них знаходиться нижче.

## Cellar ППІ (Прикладний Програмний Інтерфейс)

Cellar ППІ - це основна частина програми.
Це RESTful (передача репрезентативного стану) ППІ, який оркеструє секретне створення, доступ та видалення.

Для отримання додаткової інформації про ППІ див [Довідник ППІ][cellar-api-ref]

Щоб допомогти з цим проектом, загляньте на [сторінку проекту Cellar ППІ][cellar-api]


## Cellar Інтерфейс (Cellar UI)

Cellar Інтерфейс - це веб-програма, яка взаємодіє з Cellar API Щоб створити, ділитись, і видалити секрети.

Для отримання додаткової інформації про Сellar Інтерфейс див. [Розділ Використання]({{<ref "usage">}}).

Щоб допомогти з цим проектом, загляньте на [сторінку проекту Cellar UI][cellar-ui]

## Система Зберігання Даних (Datastore)

Вимога "Datastore" - це місце, де Cellar зберігає свої метадані та зашифрований секретний вміст.
-- Щоб працювати з Cellar, системі потрібно мати спосіб створювати, читати, оновлювати та видаляти дані.
Також, система має підтримати створення даних із закінченням терміну дії, після того дані буде автоматично видалено.

На даний момент Cellar підтримує:

- [Redis][redis]

## Криптографія (Cryptography)

Cellar використовує вимогу "Cryptography" щоб шифрувати секретний вміст.
Він повинен підтримувати шифрування та дешифрування має відповідати зашифрованому або розшифрованому вмісту.

На даний момент Cellar підтримує:

- [HashiCorp Vault][vault]
- [AWS KMS][aws-kms]


[priv-bin]: https://github.com/PrivateBin/PrivateBin
[cellar-api]: https://gitlab.com/auroq/cellar/cellar-api
[cellar-api-docs]: https://gitlab.com/auroq/cellar/cellar-api
[cellar-api-ref]: https://cellar-app.gitlab.io/cellar-api/
[cellar-ui]: https://gitlab.com/auroq/cellar/cellar-ui
[redis]: https://redis.io/
[vault]: https://www.vaultproject.io/
[aws-kms]: https://aws.amazon.com/kms/
