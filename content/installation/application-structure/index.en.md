+++
title = "Application Structure"
date = 2020-11-28T23:55:59Z
weight = 1
+++

Cellar consists of four pieces.
A description of each is found below.

## Cellar API

The Cellar API is the core piece of the application.
It is a RESTful API that handles orchestrates secret creation, access and deletion.

For more information about the API see the [API Reference][cellar-api-ref]

Or to contribute, take a look at the [API project page][cellar-api]

## Cellar UI

The Cellar UI is a web app that interacts with the Cellar API to orchestrates secret creation, access and deletion.

For more inforamtion about the UI see the [Usage Chapter]({{% ref "usage" %}}).

Or to contribute, take a look at the [UI project page][cellar-ui]

## Datastore

The "Datastore" dependency is where Cellar stores its metadata and encrypted secret content.
The datastore must allow creating, reading, updating, and deleting data.
Additionally, the datastore must handle creating data with an expiration along with automatic deletion when data
expires.

The following datastores are currently supported:

- [Redis][redis]

## Cryptography

The "Cryptography" dependency is an encryption as a service platform that Cellar uses to encrypt secret content.
It must support encrypting and decrypting and must respond with the encrypted or decrypted content.

The following encryption as a service platforms are currently supported:

- [Hashicorp Vault][vault]
- [AWS KMS][aws-kms]

[priv-bin]: https://github.com/PrivateBin/PrivateBin

[cellar-api]: https://gitlab.com/auroq/cellar/cellar-api

[cellar-api-docs]: https://gitlab.com/auroq/cellar/cellar-api

[cellar-api-ref]: https://cellar-app.gitlab.io/cellar-api/

[cellar-ui]: https://gitlab.com/auroq/cellar/cellar-ui

[redis]: https://redis.io/

[vault]: https://www.vaultproject.io/

[aws-kms]: https://aws.amazon.com/kms/
