---
title: Home
description: >-
  Cellar App
blocks:
  - heading1: Open Source
    heading2: Secret Sharing
    bg_image: /images/hero1.jpg
    template: hero

  - heading: |-
      Cellar is an application for sharing secrets in text form for a set period of time
    copy: |-
      Cellar is very simple to use. Enter your text and set an expiration. Cellar will then generate an ID for your secret and securely encrypt and store it. After the expiration you have set, the secret is automatically deleted.
    image: /images/create-a-secret-app-1.png
    template: text

  - heading: |-
      When Could I use Cellar?
    copy: |-
      Cellar is a quick way to share secrets without the need to authenticate or manage access control. It is a great option if you want quickly share information with someone without the need to create any kind of account.
    uses:
      - heading: Send Securely
        copy: Send your Social Security Number to HR when you get onboarded at your new job
      - heading: Share with Friends
        copy: Share instructions on how to get into your house to a friend while you are on vacation
      - heading: Communicate Confidently
        copy: Share your bank information securely for a wire transfer
    template: uses

  - image: /images/mockup.jpg
    caption: "Because Cellar relies on data expiration, access count limits, and randomly generated IDs to restrict access, you know that your data stayed safe in transit and is only available as long as you want it to be."
    template: image

  - heading: "Why Cellar"
    features:
      - heading: Auto Expiring
        copy: Set an expiration time or max number of times someone can view the secret
        image: /images/cellar_5.jpg
      - heading: Easy to Use
        copy: Use it anytime, without logging in or creating an account
        image: /images/cellar_6.jpg
      - heading: Self Hosted
        copy: Host it on your own server. Install it easily on digital ocean or any virtual server
        image: /images/pexels-eye4dtail-792032_02.jpg
    template: features

  - tagline: |-
      CELLAR IS TOTALLY FREE,\
      INSTALL IT YOURSELF AND FIND OUT WHAT’S POSSIBLE
    template: tagline
---
