+++
title = "Creating a Secret"
date = 2020-11-29T00:10:52Z
weight = 3
+++

Creating a secret is done from the Create Secret Page displayed below.
This is also the landing page of the application.

![create-page](/create-overview.png)

1. Start by typing your secret into the secret content text area in the middle of the page.
2. Next select when you want your secret to expire.
3. Then input an access limit for your secret or click the "NO LIMIT" button to disable this feature.
4. Finally click the create button.
5. If you missed anything, relevant error messages will appear.
   Otherwise, the secret will be created, and you will be redirected to the [Secret Metadata page]({{% ref
   secret-metadata %}}).

## Expiration

The expiration is the time frame in which your secret will be deleted.
It must be at least 10 minutes in from the time the secret is created.
Once a secret has expired all trace of it will be removed and it cannot be accessed or recovered.

There are two ways to select an expiration:

### Absolute

An absolute expiration is the most straightforward.

![create-expiration-absolute.png](/create-expiration-absolute.png)

- First select the "Expire on (Absolute)" option from the section labeled "Expiration".
- Next select an exact date and time for the secret to expire.
- Time is set in your current time zone and can be selected in half-hour intervals.

### Relative

Specifying a relative expiration causes the secret to expire after a certain duration relative to the secret's creation.
For example, selecting 12 hours as a relative expiration will cause a secret to expire exactly 12 hours after it was
created.

![create-expiration-relative.png](/create-expiration-relative.png)

- Start by selecting the "Expire after (Relative)" option from the section labeled "Expiration".
- Then enter an even number of hours and minutes that you want the secret to exist.

## Access Limit

The access limit is the number of times your secret content can be accessed.
Accessing secret metadata (see below) does not count against this limit.

Once a secret has been accessed the specified number of times,
all trace of it will be removed, and it cannot be accessed or recovered.
This deletion will occur even if secret's expiration has not been reached.

![create-access-limit.png](/create-access-limit.png)

To set an access limit, enter an even number into the box labeled access limit.

This option can also be disabled.
If it is disabled, the secret's content can be accessed an unlimited number of times until it expires.

If you would like to disable the access limit, simply click the "NO LIMIT" button that appears next to the access
limit field.

