+++
title = "Accessing a Secret"
date = 2020-11-29T00:11:13Z
weight = 7
+++

To access a secret you will need a link generated by cellar when the secret is created.
In the majority of cases, this link will be sent to you by someone who has created a secret they would like to share with you.
This link will look similar to the following:

`https://cellar.example.com/secret/440028a9b535aa74dae7401e0def34d2fffc861afd496141b0b0b1b00a8e8466/access`

To learn how to create a secret, refer to the [Creating a Secret]({{% ref "creating-a-secret" %}}).

Once you have received a link to access a secret, simply navigate to the provided URL.
You will find a page similar to the one depicted below:

![access-overview.png](/access-overview-trimmed.png)

The content of the secret will be displayed in the text box in the middle of the screen.
Pressing the "Copy Secret" button will copy the full contents of the secret to your clipboard.

## No Secrets Here

If while accessing secret content, you are redirected to the page below,
that means the secret does not exist.
Most likely the secret has expired or reached its access limit.
Or perhaps the secret creator has chosen to delete it.

![secret-not-found.png](/secret-not-found.png)
