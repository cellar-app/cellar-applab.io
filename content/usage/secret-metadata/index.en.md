+++
title = "Secret Metadata"
date = 2020-11-29T00:11:02Z
weight = 5
+++

The Secret Metadata page is useful for keeping track of information about a secret you have created.
This page contains everything about the secret except the actual content of the secret.

![metadata-overview.png](/metadata-overview-trimmed.png)

The most important part of the page is the "Copy Link to Secret" button.
Pressing this button will generate a URL to access the secret and place it in your clipboard.
Send this link out to anyone with whom you would like to share your secret.
See [Accessing a Secret]({{% ref "accessing-a-secret" %}})

The "Copy Link to Metadata" button will copy the URL to this Secret Metadata page to your clipboard.
Since this website does not store any cookies or use any accounts to create secrets,
you will need that URL to access this page again.

In the top left of the page you will find the date and time when the secret will expire.
Next to it is a count of how many times the secret content has been accessed.
It will also show the access limit if one has been set.
Refer to [Creating a Secret]({{% ref "creating-a-secret" %}}) for more information on what these values mean.

You can also find the ID (or part of the ID) of the secret in the center of the page.

To the right of the copy buttons is the delete button.
Use this button to delete your secret at any time.
This action cannot be reversed.
Once you have deleted the secret it can no longer be accessed or recovered.

## No Secrets Here

If while accessing the metadata page for a secret, you are redirected to the page below,
that means the secret (and therefore its metadata) does not exist.
Most likely the secret has expired or reached its access limit.
Or perhaps it has simply been deleted.

![secret-not-found.png](/secret-not-found.png)
