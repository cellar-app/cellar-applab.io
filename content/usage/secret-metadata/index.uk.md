+++
title = "Метадані Секрету"
date =  2020-11-29T00:11:02Z
weight = 5
+++

Ви можете використовувати сторінку "Метадані Секрета" щоб відстежити інформацію про секрет який ви створили.
На ці сторінці є вся інформацію про секрет крім сами вміст секрету.

![metadata-overview.png](/metadata-overview-trimmed.png)

Найважливіша частина цої сторінки - це кнопка "Copy Link to Secret".
Натисніть цю кнопку щоб створити і скопіювати до буфера ярлик який дає доступ до вмісту секрету.
надсилайте це ярлик тому, з ким ви хочете ділитися вміст секрету.
див сторінку [Доступ До Секрету]({{< ref "accessing-a-secret" >}})

Натисніть кнопку "Copy Link to Metadata" щоб скопіювати до буфера ярлик від цої сторінки.
Через те що вебсайт Cellar працює без куки і без акаунти,
вам буде потрібно зберігати цей ярлик щоб знову знайти цю сторінку.

У середині сторінки ви знайдете дату та час, коли секрет стане недоступно.
Поруч із ним є номер. Це скільки разів здійснювалося доступ до вмісту секрету.
Якщо ви встановили ліміт на стільки, скільки разів дозволено переглядати вміст секрету, це номер також буде там.
Див сторінку [Створення Секрету]({{< ref "creating-a-secret" >}}) за додаткову інформацію щодо значення цих номерів.

Ви також можете знайти ідентифікаційний код секрету (або частину цього) у верхньому правому куті сторінки.

Під ідентифікаційним кодом є кнопка "delete".
Натисніть цю кнопку коли-небудь щоб видалити ваш секрет.
**Цю дію неможливо скасувати.**
**Після того, як ви видалили секрет, до нього більше не можна отримати доступ або відновити вміст.**


## No Secrets Here

Якщо ви побачите сторінку вниз,
це значит що вже немає секрет (і теж метадані) за цим ідентифікаційним кодом.
Напевно секрет переглядали стільки, скільки разів дозволено.
Чи можливо хтось видалив цей секрет.

![secret-not-found.png](/secret-not-found.png)
